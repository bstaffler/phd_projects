PhD Projects
============

This repository contains references to projects I worked on during my time at
the Department of Connectomics in the Max Planck Institute for Brain Research
([http://brain.mpg.de](http://brain.mpg.de)).


SynEM - Automated Synapse Detection for Connectomics
----------------------------------------------------

[SynEM](https://elifesciences.org/articles/26414) [1] is a method for synapse detection in 3D electron microscopy datasets
that detects synapses by classifying interfaces between neighboring neurites.

* SynEM website: http://synem.rzg.mpg.de/
* Paper at eLife: https://elifesciences.org/articles/26414
* Code repository: https://gitlab.mpcdf.mpg.de/connectomics/SynEM/


Synapse detection by signed proximity estimation and pruning
------------------------------------------------------------

The method developed by [Parag et al.](https://arxiv.org/abs/1807.02739) [2] first detects synapse candidates including their orientation from voxelwise predictions of a deep neural network (3D unet).
The synapse candidates are then combined with a segmentation by overlap and a second 3D CNN is used to prune false positive connections.

* Preprint: https://arxiv.org/abs/1807.02739
* Code repository: https://github.com/paragt/EMSynConn


Codat
-----

A tensorflow library for semantic segmentation of 3D data (voxelwise classification).

* Repository: https://gitlab.mpcdf.mpg.de/connectomics/codat


References
----------

[1] Staffler, B., Berning, M., Boergens, K. M., Gour, A., van der Smagt, P., & Helmstaedter, M. (2017). SynEM, automated synapse detection for connectomics. Elife, 6, e26414.

[2] Parag, T., Berger, D., Kamentsky, L., Staffler, B., Wei, D., Helmstaedter, M., Lichtman, J. W. & Pfister, H. (2018). Detecting Synapse Location and Connectivity by Signed Proximity Estimation and Pruning with Deep Nets. arXiv preprint arXiv:1807.02739.
